#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 17:11:39 2019

@author: aditya
"""

import numpy as np
import pandas as pd

dataset=pd.read_csv("/home/aditya/dataset.csv")
x=dataset.iloc[:,:-1]
y=dataset.iloc[:,-1]

from sklearn.preprocessing import LabelEncoder
le=LabelEncoder()
x=x.apply(le.fit_transform)
print(x)
from sklearn.tree import DecisionTreeClassifier
model=DecisionTreeClassifier()
model.fit(x.iloc[:,1:5],y)
y_pred=model.predict([[1,1,0,0]])
print(y_pred)

from sklearn.externals.six import StringIO
from sklearn.tree import export_graphviz
import pydotplus

dot_data=StringIO()
export_graphviz(model,out_file=dot_data,filled=True,special_characters=True)
graph=pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_png("1.png")