#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 18:20:38 2019

@author: aditya
"""

import numpy as np
import pandas as pd

x=np.array([[0.1,0.6],[0.15,0.71],[0.08,0.9],[0.16,0.85],[0.2,0.3],[0.25,0.5],[0.24,0.1],[0.3,0.2]])
x.shape
from sklearn.cluster import KMeans
centroid=np.array([[0.1,0.6],[0.3,0.2]])
model=KMeans(n_clusters=2,init=centroid,n_init=1)
model.fit(x)
labels=model.labels_
print(labels)
x_test=np.array([[0.25,0.5]])
y_pred=model.predict(x_test)
print("\nP6 belongs to : ",y_pred[0])
count=0
for i in labels:
    if i ==1:
        count+=1

print("the population of cluster around m2 : ",count)
print("the updated centroid is : \n",model.cluster_centers_[0],"\n",model.cluster_centers_[1])

import matplotlib.pyplot as plt

plt.scatter(x[:,0],x[:,1])
plt.title("INITIAL DATA")
plt.show()
plt.scatter(x[:,0],x[:,1],c=labels,cmap='rainbow')
plt.scatter(model.cluster_centers_[:,0],model.cluster_centers_[:,1],s=200,alpha=0.5)
plt.title("clusteres")








