#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 14:31:17 2019

@author: aditya
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

dataset=pd.read_csv("/home/aditya/hours.csv")
x=dataset.iloc[:,:-1].values
print(x)
y=dataset.iloc[:,-1].values
from sklearn.linear_model import LinearRegression,LogisticRegression
from sklearn.preprocessing import PolynomialFeatures
poly=PolynomialFeatures(degree=3)

x_poly=poly.fit_transform(x)
lin2=LinearRegression()
lin2.fit(x_poly,y)
print(x_poly)



log=LogisticRegression(solver='lbfgs',multi_class='auto',max_iter=2000)
log.fit(x,y)
model=LinearRegression()
model.fit(x,y)
accuracy=model.score(x,y)*100
print(accuracy)
y_pred=model.predict([[11]])
print("predicted",y_pred)
print(model.coef_)
print(model.intercept_)
print(lin2.coef_)
print(lin2.intercept_)
eq=model.coef_*y+model.intercept_
print("equation of line : \n",model.coef_[0],"* y + ",model.intercept_)
print("risk score : ",eq[0])
plt.plot(x,y,"o")
plt.show()
plt.plot(x,y,"o")
plt.plot(x,model.predict(x))
plt.title("LINEAR REGRESSION")
plt.show()
plt.scatter(x,y)
plt.plot(x,lin2.predict(x_poly),c='red')
plt.title("POLYNIMIAL REGRESSION")
plt.show()
plt.scatter(x,y)
plt.plot(x,log.predict(x),c='red')
plt.title("logistic REGRESSION")

