#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 13:35:09 2019

@author: aditya
"""

from sklearn.datasets import load_iris
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

model=LogisticRegression(solver='lbfgs',multi_class='auto',max_iter=1000)




dataset=load_iris()
x=dataset.data
y=dataset.target

x_train,x_test,y_train,y_test=train_test_split(x,y,test_size=0.3)

model.fit(x_train,y_train)
y_pred=model.predict(x_test)

print("confusion matrix :\n ",confusion_matrix(y_test,y_pred))
print("accuracy : \n",accuracy_score(y_test,y_pred))
print("classification report : \n",classification_report(y_test,y_pred))