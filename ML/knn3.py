#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 13:49:21 2019

@author: aditya
"""

import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier


dataset = pd.read_csv("/home/aditya/rdata.csv")
print(dataset.shape) 
x=dataset.iloc[:,:-1].values
print(x)
y=dataset.iloc[:,-1].values
print(y)
model=KNeighborsClassifier(n_neighbors=3)
model.fit(x,y)
x_test=np.array([[6,6]])
pred=model.predict(x_test)
print(pred)

model=KNeighborsClassifier(n_neighbors=3,weights='distance')
model.fit(x,y)
pred=model.predict([[6,4]])
print(pred)
print(model.score(x,y))