#Key Generation 

Rcons1='10000000'
Rcons2='00110000'

sbox=[[9,4,10,11],
       [13,1,8,5],
       [6,2,0,3],       
       [12,14,15,7]]

def htob(a):
	dec=int(a,16)
	bin1=bin(dec)[2:].zfill(16)
	return bin1

def left_half(bits):
	return bits[:int(len(bits)/2)]

def right_half(bits):
	return bits[int(len(bits)/2):]

def xor(b1,b2):
	new=''
	for bits1,bits2 in zip(b1,b2):
		new+=str((int(bits1)+int(bits2))%2)
	return new


def look_up_in_sbox(bits):
	row= int((bits[0]+bits[1]),2)
	col= int((bits[2]+bits[3]),2)
	return '{0:04b}'.format(sbox[row][col])

def RotNib(bits):
	L=left_half(bits)
	R=right_half(bits)
	bits=R+L
	return bits

def SubNib(bits):
	if(len(bits)==8):
		L=left_half(bits)
		R=right_half(bits)
		x=look_up_in_sbox(L)
		y=look_up_in_sbox(R)
		return (x+y)

	else:
		L=left_half(bits)
		R=right_half(bits)
		l1=left_half(L)
		l2=left_half(R)
		r1=right_half(L)
		r2=right_half(R)
		x1=look_up_in_sbox(l1)
		x2=look_up_in_sbox(l2)
		y1=look_up_in_sbox(r1)
		y2=look_up_in_sbox(r2)
		return (x1+y1+x2+y2)
	
key=input("Enter the 16-bit key(Hex format):")
key=htob(key)
print("Key in binary Format:",key)		
print("------------------------------------------")
print("Key Generation")
w0=left_half(key)					
print("w0:",w0)
w1=right_half(key)
print("w1:",w1)
w2=xor(w0,xor(Rcons1,SubNib(RotNib(w1))))
print("w2:",w2)
w3=xor(w2,w1)
print("w3:",w3)
w4=xor(w2,xor(Rcons2,SubNib(RotNib(w3))))
print("w4:",w4)
w5=xor(w3,w4)
print("w5:",w5)	
print("------------------------------------------")		
print("Key Generated")
key0=w0+w1	
key1=w2+w3
key2=w4+w5
print("Key0:",key0)
print("Key1:",key1)
print("Key2:",key2)
print("------------------------------------------")

