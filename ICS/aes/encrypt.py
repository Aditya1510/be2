from kg import *

m=[[0,0],[0,0]]
def Shr(bits):
	L=left_half(bits)
	R=right_half(bits)
	l1=left_half(L)
	l2=right_half(R)
	l1=left_half(L)
	l2=right_half(L)
	r1=left_half(R)
	r2=right_half(R)
	return (l1+r2+r1+l2)
	
def gf(p1,p2):
	'''Multiply two polynomials in GF (2^4)/x^4+x+1'''
	p = 0
	p3=int(p2)
	while p3:
		if p3 & 0b1:
			p ^= p1
		p1 <<= 1
		if p1 & 0b10000:
			p1 ^= 0b11
		p3 >>= 1
	return p & 0b1111

def mul_matrix(mat):
	return [gf(1,int(mat[0][0],2))^gf(4,int(mat[1][0],2)),gf(1,int(mat[0][1],2))^gf(4,int(mat[1][1],2)),gf(4,int(mat[0][0],2))^gf(1,int(mat[1][0],2)),gf(4,int(mat[0][1],2))^gf(1,int(mat[1][1],2))]
def btoh(bits):
	dec=int(bits,2)
	#print("Binary:",dec)
	hex1=hex(dec)[2:]
	return hex1

def encrypt(bits):
	print("------------------------------------------")
	R0=xor(key0,P)
	print("R0 is:",R0)
	s1=SubNib(R0)
	print("After First Nibble Substitution:",s1)
	shr1=Shr(s1)
	print("After First Shift Row:",shr1)
	L=left_half(shr1)
	R=right_half(shr1)
	l1=left_half(L)
	l2=right_half(L)
	r1=left_half(R)
	r2=right_half(R)
	nmat=[[l1,r1],[l2,r2]]
	print(nmat)
	p=mul_matrix(nmat)
	m[0][0]='{0:04b}'.format(p[0])
	m[0][1]='{0:04b}'.format(p[1])
	m[1][0]='{0:04b}'.format(p[2])
	m[1][1]='{0:04b}'.format(p[3])
	print("Matrix after mix Column:\n",m)
	temp=m[0][0]+m[1][0]+m[0][1]+m[1][1]
	print("After Mix Column=",temp)
	R1=xor(temp,key1)
	print("R1 is:",R1)
	s2=SubNib(R1)
	print("After Second Nibble Substitution:",s2)
	shr2=Shr(s2)
	print("After Second Shift Row:",shr2)
	R2=xor(shr2,key2)
	print("Cipher Text:",R2)
	h1=btoh(R2)
	print("Cipher Text in Hex format:",h1)
	print("------------------------------------------")

P=input("Enter the 16 bit message(Hex Format):")
P=htob(P)
print("Plaintext in binary format:",P)
encrypt(P)
