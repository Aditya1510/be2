#Decryption

from kg import key0,key1,key2,left_half,right_half,htob,xor

m=[[0,0],[0,0]]
isbox=[[10,5,9,11],
       [1,7,8,15],
       [6,0,2,3],
       [12,4,13,14]]

def btoh(bits):
	dec=int(bits,2)
	hex1=hex(dec)[2:]
	return hex1


		
def IShr(bits):
	L=left_half(bits)
	R=right_half(bits)	
	l1=left_half(L)
	l2=right_half(L)
	r1=left_half(R)
	r2=right_half(R)
	return l1+r2+r1+l2

def look_up_in_isbox(bits):
	row=int(bits[0]+bits[1],2)
	col=int(bits[2]+bits[3],2)
	return '{0:04b}'.format(isbox[row][col])

def ISubNib(bits):
	if(len(bits)==8):
		L=left_half(bits)
		R=right_half(bits)
		x=look_up_in_sbox(L)
		y=look_up_in_sbox(R)
		return (x+y)

	else:
		L=left_half(bits)
		R=right_half(bits)
		l1=left_half(L)
		l2=left_half(R)
		r1=right_half(L)
		r2=right_half(R)
		x1=look_up_in_isbox(l1)
		x2=look_up_in_isbox(l2)
		y1=look_up_in_isbox(r1)
		y2=look_up_in_isbox(r2)
		return (x1+y1+x2+y2)

def gf(p1, p2):
    """Multiply two polynomials in GF(2^4)/x^4 + x + 1"""
    p = 0
    p3=int(p2)
    while p3:
        if p3 & 0b1:
            p ^= p1
        p1 <<= 1
        if p1 & 0b10000:
            p1 ^= 0b11
        p3 >>= 1
    return p & 0b1111
 		
def mult_matrix(mat):
	return[gf(9,int(mat[0][0],2))^gf(2,int(mat[1][0],2)),gf(9,int(mat[0][1],2))^gf(2,int(mat[1][1],2)),gf(2,int(mat[0][0],2))^gf(9,int(mat[1][0],2)),gf(2,int(mat[0][1],2))^gf(9,int(mat[1][1],2))]

def decrypt(bits):
	print("------------------------------------------")	
	R0=xor(C,key2)
	print("R0 is:",R0)
	ishr1=IShr(R0)
	print("After First Inverse Shift Row:",ishr1)
	is1=ISubNib(ishr1)
	print("After Fist Inverse Nibble Substitution:",is1)
	R1=xor(is1,key1)
	print("R1 is:",R1)	
	L=left_half(R1)
	R=right_half(R1)
	l1=left_half(L)
	l2=right_half(L)
	r1=left_half(R)
	r2=right_half(R)	
	nmat=[[l1,r1],[l2,r2]]
	p=mult_matrix(nmat)
	m[0][0]='{0:04b}'.format(p[0])
	m[0][1]='{0:04b}'.format(p[1])	
	m[1][0]='{0:04b}'.format(p[2])
	m[1][1]='{0:04b}'.format(p[3])
	print("Matrix after mix Column:\n",m)
	temp=m[0][0]+m[1][0]+m[0][1]+m[1][1]
	print("After Mix Column=",temp)
	ishr2=IShr(temp)
	print("After Second Inverse Shift Row:",ishr2)		
	is2=ISubNib(ishr2)
	print("After Second Nibble Substitution:",is2)
	R2=xor(is2,key0)
	print("Plain Text:",R2)
	h1=btoh(R2)
	print("Plain Text in Hex format:",h1)
	print("------------------------------------------")

C=input("Enter the Cipher text (Hex Format):")
C=htob(C)
print("Cipher text in Binary Format:",C)
decrypt(C)
