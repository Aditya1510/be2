fixed_ip = [2, 6, 3, 1, 4, 8, 5, 7]
fixed_ep = [4, 1, 2, 3, 2, 3, 4, 1]
fixed_ip_inverse = [4, 1, 3, 5, 7, 2, 8, 6]
fixed_p10 = [3, 5, 2, 7, 4, 10, 1, 9, 8, 6]
fixed_p8 = [6, 3, 7, 4, 8, 5, 10, 9]
fixed_p4 = [2, 4, 3, 1]

s0 = [[1, 0, 3, 2],
      [3, 2, 1, 0],
      [0, 2, 1, 3],
      [3, 1, 3, 2]]

s1 = [[0, 1, 2, 3],
      [2, 0, 1, 3],
      [3, 0, 1, 0],
      [2, 1, 0, 3]]

key='1010000010'

def permutate(original,fixed_key):
	new=''
	for i in fixed_key:
		new += original[i-1]
	return new

def right_half(bits):
	return bits[int(len(bits)/2):]

def left_half(bits):
	return bits[:int(len(bits)/2)]


def shift(bits):
	rotated_left_half=left_half(bits)[1:]+left_half(bits)[0]
	rotated_right_half=right_half(bits)[1:]+right_half(bits)[0]
	return rotated_left_half+rotated_right_half


def key1():
	return permutate(shift(permutate(key,fixed_p10)),fixed_p8)

def key2():
	return permutate(shift(shift(permutate(key,fixed_p10))),fixed_p8)


def xor(bits,key):
	new=''
	for bit,key_bit in zip(bits,key):
		new+=str((int(bit)+int(key_bit))%2)
	return new
	
def lookup_in_sbox(bits,sbox):
	print(len(bits))
	row=int(bits[0]+bits[3],2)
	col=int(bits[1]+bits[2],2)
	return "{0:02b}".format(sbox[row][col])
	

def f_k(bits,key):
	L=left_half(bits)
	R=right_half(bits)
	bits=permutate(R,fixed_ep)
	print("bits after extended permutation are : ",bits)
	bits=xor(bits,key)
	print("bits after xor permutation : ",bits)
	bits=lookup_in_sbox(left_half(bits),s0)+lookup_in_sbox(right_half(bits),s1)
	print("o/p of sbox is : ",bits)
	bits=permutate(bits,fixed_p4)
	print("bits after p4 permutation : ",bits)
	return xor(bits,L)
	
		
	


def encrypt(plain_text):
	key1_val=key1()
	print("value of key1 : ",key1_val)
	key2_val=key2()
	print("value of key2 : ",key2_val)
	
	bits=permutate(plain_text,fixed_ip)
	print("bits after initial permutation : ",bits)
	print("encrypting using key1.")
	temp=f_k(bits,key1_val)
	bits=right_half(bits)+temp
	print("********************************")
	print("encrypting using key 2")
	bits=f_k(bits,key2_val)
	print("encrypted value is : ")
	print(permutate(bits+temp,fixed_ip_inverse))
	
encrypt('11110011')























