#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 20:01:29 2019

@author: aditya
"""

def isprime(num):
    return 2 in [num,2**num%num]

def gcd(a,b):
    while b!=0:
        a,b=b,a%b
    return a
    
def modinverse(e,phi):
    for i in range(1,phi):
        d=float((phi*i+1)/e)
        if(d.is_integer()):
            return int(d)
    return None

def coprimes(phi):
    l=[]
    for i in range(2,phi):
        if gcd(i,phi)==1:
            l.append(i)
        
    return l


while True:
    p=int(input("enter value of p : "))
    if isprime(p):
        break
    else:
        print("entered number is not prime. Please try again")
        
while True:
    q=int(input("enter value of q : "))
    if isprime(q):
        break
    else:
        print("entered number is not prime. Please try again")
        
n=p*q
phi=(p-1)*(q-1)
print("enter value of e from folowing numbers : \n",coprimes(phi))
while True:
    e=int(input())
    if e in coprimes(phi):
        break
    else:print("please enter number from given list :")
d=modinverse(e,phi)
print("public key is {%d,%d}"%(e,n))
print("private key is {%d,%d}"%(d,n))

while True:
    plain=int(input("enter plain text :"))
    if plain<n:
        break
    else:print("p should be less than n ",n)
        
c=plain**e % n
print("cipher text : ",c)

print("decrypted text is : ",c**d%n)

