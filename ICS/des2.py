fixed_ip = [2, 6, 3, 1, 4, 8, 5, 7]
fixed_ep = [4, 1, 2, 3, 2, 3, 4, 1]
fixed_ip_inverse = [4, 1, 3, 5, 7, 2, 8, 6]
fixed_p10 = [3, 5, 2, 7, 4, 10, 1, 9, 8, 6]
fixed_p8 = [6, 3, 7, 4, 8, 5, 10, 9]
fixed_p4 = [2, 4, 3, 1]

s0 = [[1, 0, 3, 2],
      [3, 2, 1, 0],
      [0, 2, 1, 3],
      [3, 1, 3, 2]]

s1 = [[0, 1, 2, 3],
      [2, 0, 1, 3],
      [3, 0, 1, 0],
      [2, 1, 0, 3]]
      
      
key='1010000010'

def permutate(original,fixed_key):
	new=''
	for i in fixed_key:
		new+=original[i-1]
	return new

def right_half(bits):
	return(bits[int(len(bits)/2):])

def left_half(bits):
	return(bits[:int(len(bits)/2)])


def shift(bits):
	shifted_left_half=left_half(bits)[1:]+left_half(bits)[0]
	shifted_right_half=right_half(bits)[1:]+right_half(bits)[0]
	return shifted_left_half+shifted_right_half



def key1():
	return permutate(shift(permutate(key,fixed_p10)),fixed_p8)	
	
def key2():
	return permutate(shift(shift(permutate(key,fixed_p10))),fixed_p8)

def xor(bits,key):
	new=''
	for a,b in zip(bits,key):
		new+=str((int(a)+int(b))%2)
	return new

def sbox_lookup(bits,s):
	row=int(bits[0]+bits[3],2)
	col=int(bits[1]+bits[2],2)
	
	return "{0:02b}".format(s[row][col])


def f_k(bits,key):
	L=left_half(bits)
	R=right_half(bits)
	bits=permutate(R,fixed_ep)
	print("expanded permutation is : ",bits)
	bits=xor(bits,key)
	print("bits after xor is : ",bits)
	bits=sbox_lookup(left_half(bits),s0)+sbox_lookup(right_half(bits),s1)
	print("after sbox lokup : ",bits)
	bits=permutate(bits,fixed_p4)
	return xor(L,bits)
	
def encrypt(plain_text):
	key1_val=key1()
	key2_val=key2()
	bits=permutate(plain_text,fixed_ip)
	temp=f_k(bits,key1_val)
	bits=right_half(bits)+temp
	bits=f_k(bits,key2_val)
	encrypted_text=permutate(bits+temp,fixed_ip_inverse)
	print("encrypted value is : ",encrypted_text)

def decrypt(cypher_text):
	bits=permutate(cypher_text,fixed_ip)
	temp=f_k(bits,key2())
	bits=right_half(bits)+temp
	bits=f_k(bits,key1())
	decrypted_text=permutate(bits+temp,fixed_ip_inverse)
	print("decrypted text is : ",decrypted_text)

encrypt('11110011')
print("********************************")
decrypt('10000101')

